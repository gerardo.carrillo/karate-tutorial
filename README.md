# karate-tutorial

karate tutorial with maven and JUnit 5

## Getting started

This project is for every one who wants to start with an easy example using karate framework to automate test API Rest.

##Instalation

To run this project you must to install:

	-Java (jdk)

Must be configured

	-JAVA_HOME

## Run project

To run this project you need to run in terminal:

	mvn test -Dtest=karateRunner

