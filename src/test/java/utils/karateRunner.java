package utils;
import com.intuit.karate.Results;
import com.intuit.karate.Runner;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class karateRunner {
    @Test
    void runTests() {
        Results results = Runner.path("classpath:features").parallel(2);
        assertEquals(0, results.getFailCount(), results.getErrorMessages());
    }
}